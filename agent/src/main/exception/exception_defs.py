import json

from web.webapi import HTTPError


class DefaultError(HTTPError):
    def __init__(self, message=None):
        status = '500 Internal Server Error'
        headers = {'Content-Type': 'application/json'}
        HTTPError.__init__(self, status, headers, message or self.message)


class InvalidRequest(DefaultError):
    def __init__(self, message):
        self.message = json.dumps({"returnCode": 1, "stdout": "", "stderr": message})
        print("Invalid Request: " + self.message)
        DefaultError.__init__(self, self.message)


class UnsupportedLanguage(DefaultError):
    def __init__(self, message):
        self.message = json.dumps({"returnCode": 2, "stdout": "", "stderr": message})
        print("Compilation Error: " + self.message)
        DefaultError.__init__(self, self.message)


class CompilationError(DefaultError):
    def __init__(self, message):
        self.message = json.dumps({"returnCode": 3, "stdout": "", "stderr": message})
        print("Compilation Error: " + self.message)
        DefaultError.__init__(self, self.message)


class UnitTestsError(DefaultError):
    def __init__(self, message):
        self.message = json.dumps({"returnCode": 4, "stdout": "", "stderr": message})
        print("Unit test error: " + self.message)
        DefaultError.__init__(self, self.message)


class OutOfMemoryError(DefaultError):
    def __init__(self, message):
        self.message = json.dumps({"returnCode": 5, "stdout": "", "stderr": message})
        print("Out of memory error: " + self.message)
        DefaultError.__init__(self, self.message)


class TimeoutError(DefaultError):
    def __init__(self, message):
        self.message = json.dumps({"returnCode": 6, "stdout": "", "stderr": message})
        print("Timeout error: " + self.message)
        DefaultError.__init__(self, self.message)