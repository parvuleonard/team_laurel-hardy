package com.hp.tools.judge.evaluators;

/**
 * @author Octavian
 * @since 23.03.2015
 */
public class PythonOutputEvaluator extends RegexOutputEvaluator {
    public PythonOutputEvaluator() {
        super("Ran (\\d) tests in failures=(\\d)");
    }
}
