package com.hp.tools.judge.evaluators;

/**
 * Created by Bogdan on 3/28/2015.
 */
public class COutputEvaluator extends RegexOutputEvaluator {
    public COutputEvaluator() {
        super("Tests run: (\\d),  Failures: (\\d)");
    }
}
